package ru.helmetoff.tm.constant;

public interface TerminalConst {

    String CMD_HELP = "help";

    String CMD_VERSION = "version";

    String CMD_ABOUT = "about";

    String WELCOME_MESSAGE = "** WELCOME TO TASK MANAGER **";

    String HELP_MESSAGE = "[HELP]\n" +
            CMD_HELP + " - Display list of terminal commands.\n" +
            CMD_VERSION + " - Display program version.\n" +
            CMD_ABOUT + " - Display developer info.";

    String VERSION_MESSAGE = "[VERSION]\n" + "0.2.0";

    String ABOUT_MESSAGE = "[ABOUT]\n" +
            "NAME: Vladislav Halmetov\n" +
            "E-MAIL: halmetoff@gmail.com";

    String WRONG_COMMAND_MESSAGE = " is invalid command. See '<program.name> help'";
}
