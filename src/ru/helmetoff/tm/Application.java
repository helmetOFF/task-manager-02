package ru.helmetoff.tm;

import ru.helmetoff.tm.constant.TerminalConst;

public class Application {

    public static void main(final String[] args) {
        displayWelcome();
        parseArgs(args);
    }

    private static void parseArgs(final String[] args){
        if(args == null || args.length == 0) return;
        final String arg = args[0];
        if(arg == null || arg.isEmpty()) return;
        switch (arg){
            case TerminalConst.CMD_HELP:
                displayHelp();
            break;
            case TerminalConst.CMD_VERSION:
                displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                displayAbout();
                break;
            default:
                displayWrongCommand(arg);
                break;
        }
    }

    private static void displayWelcome(){
        System.out.println(TerminalConst.WELCOME_MESSAGE);
    }

    private static void displayHelp(){
        System.out.println(TerminalConst.HELP_MESSAGE);
    }

    private static void displayVersion(){
        System.out.println(TerminalConst.VERSION_MESSAGE);
    }

    private static void displayAbout(){
        System.out.println(TerminalConst.ABOUT_MESSAGE);
    }

    private static void displayWrongCommand(final String arg){
        System.out.println("'"+ arg +"'"+ TerminalConst.WRONG_COMMAND_MESSAGE);
    }

}
